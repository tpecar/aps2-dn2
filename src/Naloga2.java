import java.util.Scanner;
/*
 * Uporaba omejena v skladu s pravilnikom FRI.
 * Tadej Pecar, 2015/2016
 */

/*
 * Kdorkoli bere to obupno kodo, bog s tabo.
 */
public class Naloga2
{
    public static void main(String args[])
    {
        Scanner vhod = new Scanner(System.in);
        //test();
        //System.exit(0);
        /* ponastavimo, ce slucajno uporabimo isto instanco veckrat */
        Status.ponastavi();
        
        BCD a = new BCD(vhod.nextLine());
        BCD b = new BCD(vhod.nextLine());
        
        // DEBUG - da si poenostavimo zadeve, vhod kar tu nastavimo
        /*
        BCD a = new BCD("876");
        BCD b = new BCD("8765432109876543");
        args=new String[]{"trace_","dv"};
        */
        BCD rezultat; /* rezultat mnozenja */
        
        /* 0. parameter nosi tip podatka, ki ga vrnemo (trace/count),
           1. parameter pa nacin po katerem mnozimo*/
        Status.izpis = args[0].equals("trace");
        switch(args[1])
        {
            case "os": rezultat = mnoziOsnovno(a, b); break;
            case "ru": rezultat = mnoziRusko(a, b); break;
            case "dv": rezultat = mnoziNaivno(a, b); break;
            case "ka": rezultat = mnoziKaracuba(a, b); break;
            /* ce ne gre za znano mnozenje, nimamo kaj za poceti */
            default: System.err.println("Neznan tip mnozenja!"); return;
        }
        /* izpisemo stevilo mnozenj ce smo v count nacinu */
        if(!Status.izpis)
            //System.out.print(Status.stMnozenj+", ");
            System.out.println(Status.stMnozenj);
        /* drugace izpisemo rezultat */
        else
        {
            rezultat.izpisi();
            System.out.println();
        }
    }
    /* test metod */
    public static void test()
    {
        (new BCD("00010000010000")).izpisi();
        System.out.println();
        
        (new BCD("1234567890")).krat(7).izpisi();
        System.out.println();
        (new BCD("123456789")).plus(new BCD("987654321")).izpisi();
        System.out.println();
        /* testi BCD razreda */
        BCD rez = (new BCD("999")).plus(new BCD("1")).krat(5).deljeno(2);
        rez.izpisi();
        System.out.println("\n"+rez.dolzina);
        
        /* testi mnozenj */
        //mnoziOsnovno(new BCD("4897237489798928345238923"), new BCD("56734892389748923489723")).izpisi();
        
        System.out.println();
        BCD pol[] = (new BCD("12345")).razpolovi(7);
        pol[0].izpisi();
        System.out.println();
        pol[1].izpisi();
        System.out.println("\n"+pol[0].dolzina+"\n"+pol[1].dolzina);
        
        (new BCD("123")).pot10(1).izpisi();
        System.out.println();
        
        (new BCD("0").minus(new BCD("0"))).izpisi();
        
        System.out.println("\n---\n");
    }
    /* ===================== IMPLEMENTACIJA MNOZENJ ========================= */
    /* osnovno mnozenje - vhod operanda, ki ju mnozimo, izhod rezultat
     * mnozenja */
    static BCD mnoziOsnovno(BCD a, BCD b)
    {
        BCD rezultat = new BCD();
        
        /* prvo stevilo a je konstantno, po drugem b se premikamo po stevkah
           ter te mnozimo z a, rezultat pa pristejemo prejsnim (prejsnje prej
           odmaknemo v levo ce so nenicelni)*/
        Stevka tStevkaB = b.konec;
        while(tStevkaB!=null)
        {
            if(!rezultat.jeEnako(0))
                rezultat.dodajPred((byte)0); /* zamaknemo */
            BCD zmnozek = a.krat(tStevkaB.vrednost);
            
            if(Status.izpis)
            {
                zmnozek.izpisi();
                System.out.println();
            }
            rezultat = rezultat.plus(zmnozek);
            
            tStevkaB = tStevkaB.predhodna;
        }
        /* naloga zahteva se da izpisemo toliko crtic kolikor je stevk v
           rezultatu - ceprav se mi osebno zdi to bolj smiselno narediti
           v mainu, stvar ne spada ravno tam.
           Tam bi namrec spadala ce bi bilo potrebno to narediti za vsa
           mnozenja a je iz nekega razloga to potrebno narediti le za osnovno */
        if(Status.izpis)
        {
            Stevka tStevkaRez = rezultat.zacetek;
            while(tStevkaRez!=null)
            {
                System.out.print("-");
                tStevkaRez=tStevkaRez.naslednja;
            }
            System.out.println();
        }
        return rezultat;
    }
    /* mnozenje po ruski kmecki metodi */
    static BCD mnoziRusko(BCD a, BCD b)
    {
        BCD rezultat = new BCD();
        do
        {
            if(Status.izpis)
            {
                a.izpisi();
                System.out.print(" ");
                b.izpisi();
                System.out.println(" "+a.zacetek.vrednost%2);
            }
            /* v primeru lihega prvega stevila drugo pristejemo rezultatu */
            if(a.zacetek.vrednost%2!=0)
                rezultat = rezultat.plus(b);
            
            if(a.jeEnako(1))
                a = new BCD("0"); /* pri 1 ne izvajamo dejanskega mnozenja/deljenja */
            else
            {
                a = a.deljeno(2);
                b = b.krat(2);
            }
        }
        while(!a.jeEnako(0));
        
        return rezultat;
    }

    /* metoda za mnozenje po naivnem deli in vladaj */
    static BCD mnoziNaivno(BCD a, BCD b)
    {
        /* navidezno dolzino obeh stevilo dolocimo po dolzini daljsega */
        int n = (a.dolzina>b.dolzina ? a.dolzina : b.dolzina);
        /* zaokrozimo navzgor na naslednje sodo, ce je to potrebno */
        if(n%2!=0)
            n++;
        
        /* izpisemo stevili, ki ju mnozimo */
        if(Status.izpis)
        {
            a.izpisi();
            System.out.print(" ");
            b.izpisi();
            System.out.println();
        }
        
        /* ce je katero izmed stevil 0, je rezultat 0 (ne stejemo) */
        if(a.jeEnako(0) || b.jeEnako(0))
            return new BCD();
        /* ce je katero izmed stevil le ena stevka, naredimo osnovno mnozenje
           med tem ter drugim stevilom */
        if(a.dolzina==1)
            return b.krat(a.zacetek.vrednost);
        if(b.dolzina==1)
            return a.krat(b.zacetek.vrednost);
        
        /* nismo se dosegli robnih pogojev (obe stevili imata vec kot eno
           stevko), razdelimo obe na polovici, ju rekurzivno mnozimo ter
           zmnozke uporabimo v formuli */
        BCD polA[] = a.razpolovi(n);
        BCD polB[] = b.razpolovi(n);
        
        BCD a0b0 = mnoziNaivno(polA[0], polB[0]);
        BCD a0b1 = mnoziNaivno(polA[0], polB[1]);
        BCD a1b0 = mnoziNaivno(polA[1], polB[0]);
        BCD a1b1 = mnoziNaivno(polA[1], polB[1]);
        
        return a1b1.pot10(n).plus(a0b1.plus(a1b0).pot10(n/2)).plus(a0b0);
    }
    /* metoda za mnozenje po Karacubovem deli in vladaj algoritmu */
    static BCD mnoziKaracuba(BCD a, BCD b)
    {
        /* navidezno dolzino obeh stevilo dolocimo po dolzini daljsega */
        int n = (a.dolzina>b.dolzina ? a.dolzina : b.dolzina);
        /* zaokrozimo navzgor na naslednje sodo, ce je to potrebno */
        if(n%2!=0)
            n++;
        
        /* izpisemo stevili, ki ju mnozimo */
        if(Status.izpis)
        {
            a.izpisi();
            System.out.print(" ");
            b.izpisi();
            System.out.println();
        }
        
        /* ce je katero izmed stevil 0, je rezultat 0 (ne stejemo) */
        if(a.jeEnako(0) || b.jeEnako(0))
            return new BCD();
        /* ce je katero izmed stevil le ena stevka, naredimo osnovno mnozenje
           med tem ter drugim stevilom */
        if(a.dolzina==1)
            return b.krat(a.zacetek.vrednost);
        if(b.dolzina==1)
            return a.krat(b.zacetek.vrednost);
        
        /* nismo se dosegli robnih pogojev (obe stevili imata vec kot eno
           stevko), razdelimo obe na polovici, ju rekurzivno mnozimo ter
           zmnozke uporabimo v formuli */
        BCD polA[] = a.razpolovi(n);
        BCD polB[] = b.razpolovi(n);
        
        /* edina razlika od naivnega mnozenja je dejansko v formuli, po kateri
           mnozimo - seveda ta ljubka formula zahteva od nas se odstevanje,
           tako da moramo implementirati se to */
        BCD a0b0 = mnoziKaracuba(polA[0], polB[0]);
        BCD a1b1 = mnoziKaracuba(polA[1], polB[1]);
        /* clen (a0 + a1)*(b0 + b1) */
        BCD midClen = mnoziKaracuba(polA[0].plus(polA[1]), polB[0].plus(polB[1]));
        
        return a1b1.pot10(n).plus(midClen.minus(a1b1).minus(a0b0).pot10(n/2)).plus(a0b0);
    }
}
/* globalni razred za stetje operacij ter nacin izpisa */
class Status
{
    static long stMnozenj=0;
    static boolean izpis=false;
    
    static void ponastavi()
    {
        stMnozenj=0;
    }
}

/* razred za operacijo nad celimi stevili v BCD zapisu -
   tega izdelamo kot seznam posameznih stevk, nad katerimi izvajamo operacije
   celostevilskega
    - sestevanja s poljubnim BCD
    - mnozenja s poljubno stevko
    - deljenja z 2
   
   Da si operacije poenostavimo:
    - NAVODILO naloge predpostavlja da bo vhod vedno pozitiven
      posledicno se nam ni sploh treba ukvarjati z odstevanjem
    - posamezne stevke MORAJO biti vedno pozitivne ter med
      vrednostmi 0-9
   
   Optimizacija:
    - ce pri sestevanju rezultat zahteva prenos na null naslednika, se ta
      ustvari
    - ce se pri odstevanju visja stevka (brez naslednika) odsteje v 0, se vrnemo
      v njenega predhodnika ter ju razvezemo
      (dan postopek po potrebi ponavljamo)
   */
class BCD
{
    /* atributi */
    Stevka zacetek; /* prva stevka vrste */
    Stevka konec; /* zadnja stevka vrste */
    
    boolean pozitiven=true; /* predznak stevila - zaenkrat privzeto pozitiven */
    
    int dolzina; /* potrebujemo zaradi deli&vladaj algoritmov, da vemo koliksen
                    odmik naredimo */
    
    /* konstruktor, ki za parameter dobi stevilo v obliki niza ter tega
       pretvori v interni zapis (vrsto) */
    BCD(String vhod)
    {
        /* ne pozabimo da prvo hranimo stevko z najmanjso tezo! */
        for(int tIdx=0; tIdx<vhod.length(); tIdx++)
        {
            /* ustvarimo ter povezemo novo s trenutno */
            /* nova stevka je sedaj trenutna */
            dodajPredNenicelno((byte)(vhod.charAt(tIdx)-'0'));
        }
    }
    /* prazen konstruktor ustvari BCD vrednosti 0 (vsebuje stevko 0) */
    BCD()
    {
        this("0");
    }
    
    /* ======================== OPERACIJE NAD VRSTO ========================= */
    /* ker mi dejansko bodisi dodajamo stevke na konec ali pa jih teoreticno
     * odvzemamo s konca (ce bi imeli odstevanje), gre za LIFO vrsto
     */
    /* dodaj naslednika ter ga povezi s trenutnim */
    void dodaj(byte vrednost)
    {
        Stevka naslednja = new Stevka(vrednost);
        if(zacetek==null)
            zacetek = konec = naslednja;
        else
        {
            konec.naslednja = naslednja;
            naslednja.predhodna = konec;
            konec = naslednja;
        }
        dolzina++;
    }
    void dodajNenicelno(byte vrednost)
    {
        /* mi dodajamo 0 na visje mesto, le ce je to prvic uporabljeno */
        if(vrednost!=0 || zacetek==null)
            dodaj(vrednost);
    }
    /* dodaj predhodnika ter ga povezi s trenutnim (potrebno za deljenje) */
    void dodajPred(byte vrednost)
    {
        Stevka predhodna = new Stevka(vrednost);
        if(zacetek==null)
            zacetek = konec = predhodna;
        else
        {
            zacetek.predhodna = predhodna;
            predhodna.naslednja = zacetek;
            zacetek = predhodna;
        }
        dolzina++;
    }
    void dodajPredNenicelno(byte vrednost)
    {
        /* mi dodajamo 0 na nizje mesto, le ce je obstojece stevilo
           nenicelno oz. prvic uporabljeno (tj. imamo vec kot eno stevko ali pa
           je edina stevka nenicelna) */
        if(vrednost!=0 || zacetek==null || !jeEnako(0))
        {
            /* ce je trenutna vrednost 0, namesto dodajanja stevke kar
               spremenimo njeno vrednost */
            if(jeEnako(0))
                this.zacetek.vrednost = vrednost;
            else
                dodajPred(vrednost);
        }
    }
    /* v primeru odvecnih nicel visje stevke rezultata po komplementu
       razvezemo od preostale vrste - zato ta metoda*/
    void odstrani()
    {
        Stevka predKoncem = konec.predhodna;
        predKoncem.naslednja=null;
        konec.predhodna=null;
        
        konec = predKoncem;
    }
    
    /*metode, da lazje obravnavamo stevila kot trak - uporabimo pri sestevanju*/
    Stevka naslednja(Stevka trenutna)
    {
        if(trenutna!=null)
            return trenutna.naslednja;
        else
            return null;
    }
    byte vrednost(Stevka trenutna)
    {
        if(trenutna!=null)
            return trenutna.vrednost;
        else
            return 0;
    }
    /* vrne ce je dano stevilo po vrednosti enako podani stevki */
    boolean jeEnako(int vrednost)
    {
        return this.zacetek!=null && this.zacetek == this.konec && this.zacetek.vrednost==vrednost;
    }
    
    /* za metode deli in vladaj je potrebno razpoloviti posamezno stevilo na
       2 dela - pri čemer je dolžina fiktivnega števila podana kot
       * argument.
       
       polovica[1] predstavlja visjo polovico
       polovica[0] predstavlja nizjo polovico
       da bo skladno z navodili naloge
       */
    BCD[] razpolovi(int n)
    {
        /* hodimo od zacetka do konca, pri tem dodajamo obstojece stevke v novi
         * BCD */
        BCD polovica[] = new BCD[]{new BCD(),new BCD()};
        
        /* mi moramo zdaj nekako za dejansko dolžino stevila d ter fiktivno
           dolžino (spredaj padding z 0) upostevati na tak način, da
           razpolovimo število glede na fiktivno dolžino:
           
           Za n velja, da je vecji ali enak d.
           [n/2,n] je visji del, vanj spada d-n/2 stevil
           [0,n/2] je nizji del, vanj spada n/2
           
           Dolocimo si stevce glede na podane meje ter dodamo stevke od leve
           proti desni - ce je dolzina posameznega odseka 0 ali negativna,
           odsek ze kar nastavimo na 0*/
        /* trenutna stevka tega stevila */
        Stevka tStevka = konec;
        int mid = ((n-1)/2);
        for(int tIdxStevke = dolzina-1; tIdxStevke>=0; tIdxStevke--)
        {
            if(tIdxStevke>mid)
                polovica[1].dodajPredNenicelno(tStevka.vrednost);
            else
                polovica[0].dodajPredNenicelno(tStevka.vrednost);
            tStevka = tStevka.predhodna;
        }
        return polovica;
    }
    /* ======================== ARITMETICNE OPERACIJE ======================= */
    /* ovijalca funkcija, da nastavi zacetno vrednost spremenljivke za
       obravnavanje komplementa - mi namrec pri negativnem rezultatu moramo
       ponovno narediti desetiski komplement, vendar ceprav je rezultat
       "negativen", ne smemo nad njim ponovno delati komplementa */
    BCD plus(BCD op)
    {
        return plus(op, true);
    }
    /* sestejemo to BCD stevilo s podanim ob upostevanju predznaka - vrnemo nov
       BCD objekt */
    BCD plus(BCD op, boolean obrniKomplement)
    {
        /* Sestevanje ima lahko 2 scenarija:
            - oba operanda (tj. trenutni ter podani BCD) sta enako (bodisi
              pozitivno ali negativno) predznacena - v tem primeru gre za
              obicajno sestevanje:
                gremo od najnizje proti najvisji stevki - ustvarimo novo BCD
                stevilo, izracunamo rezultat stevk tega ter operanda ter dodamo
                stevko v novi BCD - to pocnemo dokler ima vsaj kateri od
                operandov stevke
              
              predznak rezultata je enak predznaku operandov
              
              Prenos na mesto, visje od obeh operandov je veljavna stevka
              (carry).
              
            - operanda imata razlicna predznaka - v tem primeru naredimo za
              negativni operand desetiski komplement (tj. njegov devetiski
              komplement, kar je v bistvu
              999...999-[neg. operand, obravnavan kot pozitivno stevilo],
              za desetiski komplement se pristejemo 1)
              ter tega sestejemo s pozitivnim na enak nacin kot pri prvem
              
              Prenos na mesto, visje od obeh operandov je neveljavna stevka
              (overflow) - ta oznacuje, da je rezultat pozitivno stevilo - ce
              do njega ne pride, je rezultat negativno stevilo.
        */
        /* ko dobimo rezultat (se posebej v primeru overflow-a), je potrebno
           odstraniti nicelno predpono, da skrajsamo vse naslednje operacije
           nad rezultatom, ter na dolgi rok prihranimo na prostoru */
        Stevka tStevkaA = this.zacetek;
        Stevka tStevkaB = op.zacetek;
        
        /* tvorimo izhodni BCD - brez inicializiranega zacetka */
        BCD rezultat = new BCD("");
        
        /* potrebna raba komplementa - le ce razlicni predznaki */
        boolean komplement = this.pozitiven!=op.pozitiven;
        int carry=(komplement ? 1:0); /* ce se je zgodil prenos */
        /* pri komplementu se rezultat privzame za negativnega razen ce imamo
           na koncu carry */
        rezultat.pozitiven = !komplement;
        
        /* dokler imamo vsaj kaksno stevko, sestevamo */
        while(tStevkaA!=null || tStevkaB!=null)
        {
            int rez = (komplement && !this.pozitiven ? 9-vrednost(tStevkaA) : vrednost(tStevkaA))
                      + (komplement && !op.pozitiven ? 9-vrednost(tStevkaB) : vrednost(tStevkaB))
                      +carry;
            carry=rez/10;
            rez%=10;
            
            rezultat.dodaj((byte)rez);
            /* premaknemo se na naslednji stevki */
            tStevkaA = naslednja(tStevkaA);
            tStevkaB = naslednja(tStevkaB);
        }
        /* ce je po koncu sestevanja postavljen carry ter nimamo komplementa,
         * dodamo se carry rezultatu.
         * Ce imamo ob nastavljenem komplementu carry, se njegova vrednost
         * zanemari, stevilo pa obravnava za pozitivno.
         */
        if(carry>0)
        {
            if(komplement)
                rezultat.pozitiven=true;
            else
                rezultat.dodaj((byte)carry);
        }
        else
            /* ce je vrednost ostala negativna moramo narediti komplement */
            if(komplement && obrniKomplement)
            {
                /* ponovno izvedemo desetiski komplement nad dobljenim
                   rezultatom tako da ga pristejemo k 0, ne
                   smemo pa narediti iste obravnave se enkrat*/
                rezultat = rezultat.plus(new BCD(), false);
                /* rezultat je seveda negativen, le sedaj ni vec v zapisu s
                   komplementom */
            }
        /* odstranimo morebitne odvecne nicle */
        while(rezultat.zacetek!=rezultat.konec &&
              rezultat.konec.vrednost==0)
            rezultat.odstrani();
        return rezultat;
    }
    /* pomozna metoda, ki uporabi sestevanje za razliko ter se izogne
       kopiranju samo zaradi predznaka.
       DANA RESITEV JE VELJAVNA LE ZA ENONITNO IZVAJANJE, ce se bi program
       pisal vecnitno, bi bila potrebna kopija! */
    BCD minus(BCD op)
    {
        op.pozitiven = !op.pozitiven;
        BCD rezultat = this.plus(op);
        op.pozitiven = !op.pozitiven;
        return rezultat;
    }
    /* pomnozimo BCD s stevko */
    BCD krat(int op)
    {
        Stevka tStevka = this.zacetek;
        /* tvorimo izhodni BCD - brez inicializiranega zacetka */
        BCD rezultat = new BCD("");
        /* v primeru, da je katerikoli izmed operandov nic, vrnemo 0 */
        if(jeEnako(0) || op == 0)
        {
            rezultat.dodaj((byte)0);
            return rezultat;
        }
        int carry=0; /* ce se je zgodil prenos */
        
        /* dokler ne zmanjka stevk */
        while(tStevka!=null)
        {
            Status.stMnozenj++;
            
            int rez = tStevka.vrednost*op+carry;
            carry=rez/10;
            rez%=10;
            
            rezultat.dodaj((byte)rez);
            tStevka = tStevka.naslednja;
        }
        /* ce je po koncu sestevanja postavljen carry, dodamo se carry
         * rezultatu */
        if(carry>0)
            rezultat.dodaj((byte)carry);
        return rezultat;
    }
    /* celostevilsko delimo BCD */
    BCD deljeno(int op)
    {
        Stevka tStevka = this.konec;
        /* tvorimo izhodni BCD - brez inicializiranega zacetka */
        BCD rezultat = new BCD("");
        
        int ostanek=0; /* ostanek pri deljenju na posamezni stopnji */
        boolean prvi=true; /* prvih nicel NE dodajamo */
        
        while(tStevka!=null)
        {
            /* glede na nalogo sklepam, da je treba stevilo deljenj steti kot
               stevilo stevk, ki jih obdelamo */
            Status.stMnozenj++;
            
            ostanek=ostanek*10+tStevka.vrednost;
            if(ostanek>=op)
            {
                rezultat.dodajPred((byte)(ostanek/op));
                ostanek%=op;
                prvi=false;
            }
            else if(!prvi)
                rezultat.dodajPred((byte)0);
            tStevka = tStevka.predhodna;
        }
        return rezultat;
    }
    /* zamik levo, oz. mnozenje s potenco 10 - potrebujemo zaradi deli&vladaj */
    BCD pot10(int eksponent)
    {
        BCD rezultat = this.plus(new BCD()); /* naredimo kopijo */
        if(jeEnako(0))
            return rezultat;
        else
        {
            for(int i=0; i<eksponent; i++)
                rezultat.dodajPred((byte)0);
        }
        return rezultat;
    }
    /* odstevanje drugega stevila od prvega - ker odstevanje pri tej nalogi
       potrebujemo le v zelo omejenem*/
    /* izpisi stevilo na standardni izhod - hitrje kot pa ce ga poskusamo
       pretvoriti v niz, se posebno ker nimamo standardnih knjiznic na voljo */
    void izpisi()
    {
        if(!pozitiven)
            System.out.print("-");
        /* zacnemo s stevko z najvecjo tezo! */
        Stevka tStevka = konec;
        while(tStevka!=null)
        {
            System.out.print(tStevka.vrednost);
            tStevka = tStevka.predhodna;
        }
    }
    /* da jih lahko debugger prikaze v neki cloveski obliki */
    @Override
    public String toString()
    {
        String izhod = (pozitiven?"":"-");
        Stevka tStevka = konec;
        while(tStevka!=null)
        {
            izhod += tStevka.vrednost;
            tStevka = tStevka.predhodna;
        }
        return izhod;
    }
}
/* element seznama - predstavlja stevko, ki kaze na svojo levo (naslednjo) ter
   desno (prejsnjo) stevko.
   
   Ceprav grozno prostorsko potratno, bomo za vsako stevko uporabili kar 1 bajt
   (dejansko delamo unpacked BCD).
   Kazalec za nazaj imamo zaradi izpisa, ki ga moramo izvesti od stevke z
   najvecjo tezo (nazaj)
   Prav tako ga potrebujemo za deljenje. */
class Stevka
{
    byte vrednost;
    
    Stevka predhodna;
    Stevka naslednja;
    /* konstruktor */
    Stevka(byte vrednost, Stevka predhodna)
    {
        this.vrednost = vrednost;
        this.predhodna = predhodna;
    }
    Stevka(byte vrednost)
    {
        this(vrednost, null);
    }
}
/* where is your god now? */